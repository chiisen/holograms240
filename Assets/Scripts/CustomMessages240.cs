﻿using HoloToolkit.Sharing;
using HoloToolkit.Unity;
using System.Collections.Generic;
using UnityEngine;

public class CustomMessages240 : Singleton<CustomMessages240>
{
    // 代表當前的Socket連接
    NetworkConnection serverConnection;

    // 當前連接的事件監聽器，這是一個典型的適配器模式，繼承自NetworkConnectionListener
    NetworkConnectionAdapter connectionAdapter;

    // 自定義消息類型
    public enum CustomMessageID : byte
    {
        // 自己的消息從MessageID.UserMessageIDStart開始編號，避免與MessageID內置消息編號衝突
        // Cube位置消息
        CubePosition = MessageID.UserMessageIDStart,
        Max
    }

    // 消息處理代理
    public delegate void MessageCallback(NetworkInMessage msg);

    // 消息處理字典
    public Dictionary<CustomMessageID, MessageCallback> MessageHandlers { get; private set; }

    // 當前用戶在Sorket服務器中的唯一編號（自動生成）
    public long LocalUserID { get; private set; }

    protected override void Awake()
    {
        base.Awake();
        // 初始化消息處理字典
        MessageHandlers = new Dictionary<CustomMessageID, MessageCallback>();
        for (byte index = (byte)MessageID.UserMessageIDStart; index < (byte)CustomMessageID.Max; index++)
        {
            if (!MessageHandlers.ContainsKey((CustomMessageID)index))
            {
                MessageHandlers.Add((CustomMessageID)index, null);
            }
        }
    }

    void Start()
    {
        // SharingStage是Sharing組件對應的腳本，內部是對經典的Socket客戶端的封裝。
        SharingStage.Instance.SharingManagerConnected += Instance_SharingManagerConnected;
    }

    private void Instance_SharingManagerConnected(object sender, System.EventArgs e)
    {
        // 初始化消息處理器
        InitializeMessageHandlers();
    }

    // 初始化消息處理器
    private void InitializeMessageHandlers()
    {
        SharingStage sharingStage = SharingStage.Instance;

        if (sharingStage == null)
        {
            return;
        }

        // 獲取當前Socket連接
        serverConnection = sharingStage.Manager.GetServerConnection();
        if (serverConnection == null)
        {
            return;
        }

        // 初始化消息監聽
        connectionAdapter = new NetworkConnectionAdapter();
        connectionAdapter.MessageReceivedCallback += ConnectionAdapter_MessageReceivedCallback;

        // 獲取當前用戶在Socket服務器中生成的唯一編號
        LocalUserID = sharingStage.Manager.GetLocalUser().GetID();

        // 根據每個自定義消息，添加監聽器
        for (byte index = (byte)MessageID.UserMessageIDStart; index < (byte)CustomMessageID.Max; index++)
        {
            serverConnection.AddListener(index, connectionAdapter);
        }
    }

    // 接收到服務器端消息的回調處理
    private void ConnectionAdapter_MessageReceivedCallback(NetworkConnection connection, NetworkInMessage msg)
    {
        byte messageType = msg.ReadByte();
        MessageCallback messageHandler = MessageHandlers[(CustomMessageID)messageType];
        if (messageHandler != null)
        {
            messageHandler(msg);
        }
    }

    protected override void OnDestroy()
    {
        if (serverConnection != null)
        {
            for (byte index = (byte)MessageID.UserMessageIDStart; index < (byte)CustomMessageID.Max; index++)
            {
                serverConnection.RemoveListener(index, connectionAdapter);
            }
            connectionAdapter.MessageReceivedCallback -= ConnectionAdapter_MessageReceivedCallback;
        }
        base.OnDestroy();
    }

    // 創建一個Out消息（客戶端傳遞給服務端）
    // 消息格式第一個必須為消息類型，其後再添加自己的數據
    // 我們在所有的消息一開始添加消息發送的用戶編號
    private NetworkOutMessage CreateMessage(byte messageType)
    {
        NetworkOutMessage msg = serverConnection.CreateMessage(messageType);
        msg.Write(messageType);
        msg.Write(LocalUserID);
        return msg;
    }

    // 將 Cube 位置廣播給其他用戶
    public void SendCubePosition(Vector3 position, MessageReliability? reliability = MessageReliability.ReliableOrdered)
    {
        if (serverConnection != null && serverConnection.IsConnected())
        {
            // 將Cube的位置寫入消息
            NetworkOutMessage msg_ = CreateMessage((byte)CustomMessageID.CubePosition);

            msg_.Write(position.x);
            msg_.Write(position.y);
            msg_.Write(position.z);

            // 將消息廣播給其他人
            serverConnection.Broadcast(msg_,
                MessagePriority.Immediate, //立即發送
                reliability.Value, //可靠排序數據包
                MessageChannel.Default); // 默認頻道
        }
    }

    // 讀取 Cube 的位置
    public static Vector3 ReadCubePosition(NetworkInMessage msg)
    {
        // 讀取用戶編號，但不使用
        msg.ReadInt64();

        // 依次讀取XYZ，這個和發送Cube時，寫入參數順序是一致的
        return new Vector3(msg.ReadFloat(), msg.ReadFloat(), msg.ReadFloat());
    }
}