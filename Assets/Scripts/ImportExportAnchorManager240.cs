﻿using HoloToolkit.Sharing;
using HoloToolkit.Unity;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR.WSA.Persistence;
using UnityEngine.VR.WSA.Sharing;
using System;
using UnityEngine.VR.WSA;
using HoloToolkit.Unity.SpatialMapping;
using System.Text;

public class ImportExportAnchorManager240 : Singleton<ImportExportAnchorManager240>
{
    /// <summary>
    /// 建立共享坐標系過程中的各種狀態
    /// </summary>
    private enum ImportExportState
    {
        // 整體狀態
        /// <summary>
        /// 開始
        /// </summary>
        Start,
        /// <summary>
        /// 已完成
        /// </summary>
        Ready,
        /// <summary>
        /// 失敗
        /// </summary>
        Failed,
        // 本地錨點存儲器狀態
        /// <summary>
        /// 本地錨點存儲器正在初始化
        /// </summary>
        AnchorStore_Initializing,
        /// <summary>
        /// 本地錨點存儲器已初始化完成（在狀態機中）
        /// </summary>
        AnchorStore_Initialized,
        /// <summary>
        /// 房間API已初始化完成（在狀態機中）
        /// </summary>
        RoomApiInitialized,
        // Anchor creation values
        /// <summary>
        /// 需要初始錨點（在狀態機中）
        /// </summary>
        InitialAnchorRequired,
        /// <summary>
        /// 正在創建初始錨點
        /// </summary>
        CreatingInitialAnchor,
        /// <summary>
        /// 準備導出初始錨點（在狀態機中）
        /// </summary>
        ReadyToExportInitialAnchor,
        /// <summary>
        /// 正在上傳初始錨點
        /// </summary>
        UploadingInitialAnchor,
        // Anchor values
        /// <summary>
        /// 已請求數據
        /// </summary>
        DataRequested,
        /// <summary>
        /// 數據已準備（在狀態機中）
        /// </summary>
        DataReady,
        /// <summary>
        /// 導入中
        /// </summary>
        Importing
    }

    /// <summary>
    /// 當前狀態
    /// </summary>
    private ImportExportState currentState = ImportExportState.Start;

    /// <summary>
    /// 上次狀態，用來測試的，代碼在Update中
    /// </summary>
    private ImportExportState lastState = ImportExportState.Start;

    /// <summary>
    /// 當前狀態名
    /// </summary>
    public string StateName
    {
        get
        {
            return currentState.ToString();
        }
    }

    /// <summary>
    /// 共享坐標系是否已經建立完成
    /// </summary>
    public bool AnchorEstablished
    {
        get
        {
            return currentState == ImportExportState.Ready;
        }
    }

    /// <summary>
    /// 序列化坐標錨點並進行設備間的傳輸
    /// </summary>
    private WorldAnchorTransferBatch sharedAnchorInterface;

    /// <summary>
    /// 下載的原始錨點數據
    /// </summary>
    private byte[] rawAnchorData = null;

    /// <summary>
    /// 本地錨點存儲器
    /// </summary>
    private WorldAnchorStore anchorStore = null;

    /// <summary>
    /// 保存我們正在導出的錨點名稱
    /// </summary>
    public string ExportingAnchorName = "anchor-1234567890";

    /// <summary>
    /// 正在導出的錨點數據
    /// </summary>
    private List<byte> exportingAnchorBytes = new List<byte>();

    /// <summary>
    /// 共享服務是否已經準備好，這個是上傳和下載錨點數據的前提條件
    /// </summary>
    private bool sharingServiceReady = false;

    /// <summary>
    /// 共享服務中的房間管理器
    /// </summary>
    private RoomManager roomManager;

    /// <summary>
    /// 當前房間（錨點將會保存在房間中）
    /// </summary>
    private Room currentRoom;

    /// <summary>
    /// 有時我們會發現一些很小很小的錨點數據，這些往往沒法使用，所以我們設置一個最小的可信任大小值
    /// </summary>
    private const uint minTrustworthySerializedAnchorDataSize = 100000;

    /// <summary>
    /// 房間編號
    /// </summary>
    private const long roomID = 8675309;

    /// <summary>
    /// 房間管理器的各種事件監聽
    /// </summary>
    private RoomManagerAdapter roomManagerCallbacks;

    /// <summary>
    /// 錨點上傳完成事件
    /// </summary>
    public event Action<bool> AnchorUploaded;

    /// <summary>
    /// 錨點加載完成事件
    /// </summary>
    public event Action AnchorLoaded;

    private TextMesh lblMsg;
    private StringBuilder sb = new StringBuilder();
    private void debug(string msg)
    {
        Debug.Log(msg);
        sb.AppendLine(msg);
    }

    protected override void Awake()
    {
        base.Awake();

        lblMsg = GameObject.Find("FPSText").GetComponent<TextMesh>();

        // 開始初始化本地錨點存儲器
        currentState = ImportExportState.AnchorStore_Initializing;
        WorldAnchorStore.GetAsync(AnchorStoreReady);
    }

    /// <summary>
    /// 本地錨點存儲器已準備好
    /// </summary>
    /// <param name="store">本地錨點存儲器</param>
    private void AnchorStoreReady(WorldAnchorStore store)
    {
        debug("本地錨點存儲器（WorldAnchorStore）已準備好 - AnchorStoreReady(WorldAnchorStore store)");

        anchorStore = store;
        currentState = ImportExportState.AnchorStore_Initialized;
    }

    private void Start()
    {

        bool isObserverRunning = SpatialMappingManager.Instance.IsObserverRunning();
        debug("空間掃瞄狀態：" + isObserverRunning);

        if (!isObserverRunning)
        {
            SpatialMappingManager.Instance.StartObserver();
        }

        // 共享管理器是否已經連接
        SharingStage.Instance.SharingManagerConnected += Instance_SharingManagerConnected;

        // 是否加入到當前會話中（此事件在共享管理器連接之後才會觸發）
        SharingStage.Instance.SessionsTracker.CurrentUserJoined += SessionsTracker_CurrentUserJoined;
        SharingStage.Instance.SessionsTracker.CurrentUserLeft += SessionsTracker_CurrentUserLeft;
    }



    #region 共享管理器連接成功後的一系列處理

    // 共享管理器連接事件
    private void Instance_SharingManagerConnected(object sender, EventArgs e)
    {
        debug("共享管理器連接成功 - Instance_SharingManagerConnected(object sender, EventArgs e)");

        // 從共享管理器中獲取房間管理器
        roomManager = SharingStage.Instance.Manager.GetRoomManager();

        // 房間管理器的事件監聽
        roomManagerCallbacks = new RoomManagerAdapter();

        // 房間中錨點下載完成事件
        roomManagerCallbacks.AnchorsDownloadedEvent += RoomManagerCallbacks_AnchorsDownloadedEvent;
        // 房間中錨點上傳完成事件
        roomManagerCallbacks.AnchorUploadedEvent += RoomManagerCallbacks_AnchorUploadedEvent;

        // 為房間管理器添加上面的事件監聽
        roomManager.AddListener(roomManagerCallbacks);
    }

    // 房間中錨點上傳完成事件
    private void RoomManagerCallbacks_AnchorUploadedEvent(bool successful, XString failureReason)
    {
        if (successful)
        {
            debug("房間錨點上傳完成 - RoomManagerCallbacks_AnchorUploadedEvent(bool successful, XString failureReason)");

            // 房間錨點上傳成功後，空間坐標共享機制建立完成
            currentState = ImportExportState.Ready;
        }
        else
        {
            debug("房間錨點上傳失敗 - RoomManagerCallbacks_AnchorUploadedEvent(bool successful, XString failureReason)");

            // 房間錨點上傳失敗
            debug("Anchor Upload Failed!" + failureReason);
            currentState = ImportExportState.Failed;
        }

        if (AnchorUploaded != null)
        {
            AnchorUploaded(successful);
        }
    }

    // 房間中錨點下載完成事件
    private void RoomManagerCallbacks_AnchorsDownloadedEvent(bool successful, AnchorDownloadRequest request, XString failureReason)
    {
        if (successful)
        {
            debug("房間錨點下載完成 - RoomManagerCallbacks_AnchorsDownloadedEvent(bool successful, AnchorDownloadRequest request, XString failureReason)");

            // 房間錨點下載完成
            // 獲取錨點數據長度
            int datasize = request.GetDataSize();

            // 將下載的錨點數據緩存到數組中
            rawAnchorData = new byte[datasize];

            request.GetData(rawAnchorData, datasize);

            // 保存完錨點數據，可以開始準備傳輸數據
            currentState = ImportExportState.DataReady;
        }
        else
        {
            debug("錨點下載失敗！" + failureReason + " - RoomManagerCallbacks_AnchorsDownloadedEvent(bool successful, AnchorDownloadRequest request, XString failureReason)");

            // 錨點下載失敗，重新開始請求錨點數據
            MakeAnchorDataRequest();
        }
    }

    /// <summary>
    /// 請求錨點數據
    /// </summary>
    private void MakeAnchorDataRequest()
    {
        if (roomManager.DownloadAnchor(currentRoom, new XString(ExportingAnchorName)))
        {
            // 下載錨點完成
            currentState = ImportExportState.DataRequested;
        }
        else
        {
            currentState = ImportExportState.Failed;
        }
    }

    #endregion

    #region 成功加入當前會話後的一系列處理

    // 加入當前會話完成
    private void SessionsTracker_CurrentUserJoined(Session session)
    {
        SharingStage.Instance.SessionsTracker.CurrentUserJoined -= SessionsTracker_CurrentUserJoined;

        // 稍等一下，將共享服務狀態設置為正常，即可以開始同步錨點了
        Invoke("MarkSharingServiceReady", 5);
    }

    // 退出當前會話
    private void SessionsTracker_CurrentUserLeft(Session session)
    {
        sharingServiceReady = false;
        if (anchorStore != null)
        {
            currentState = ImportExportState.AnchorStore_Initialized;
        }
        else
        {
            currentState = ImportExportState.AnchorStore_Initializing;
        }
    }

    /// <summary>
    /// 將共享服務狀態設置為正常
    /// </summary>
    private void MarkSharingServiceReady()
    {
        sharingServiceReady = true;


#if UNITY_EDITOR || UNITY_STANDALONE

        InitRoomApi();

#endif

    }

    /// <summary>
    /// 初始化房間，直到加入到房間中（Update中會持續調用）
    /// </summary>
    private void InitRoomApi()
    {
        int roomCount = roomManager.GetRoomCount();

        if (roomCount == 0)
        {
            debug("未找到房間 - InitRoomApi()");

            // 如果當前會話中，沒有獲取到任何房間
            if (LocalUserHasLowestUserId())
            {
                // 如果當前用戶編號最小，則創建房間
                currentRoom = roomManager.CreateRoom(new XString("DefaultRoom"), roomID, false);
                // 房間創建好，準備加載本地的初始錨點，供其他人共享
                currentState = ImportExportState.InitialAnchorRequired;

                debug("我是房主，創建房間完成 - InitRoomApi()");
            }
        }
        else
        {
            for (int i = 0; i < roomCount; i++)
            {
                currentRoom = roomManager.GetRoom(i);
                if (currentRoom.GetID() == roomID)
                {
                    // 加入當前房間
                    roomManager.JoinRoom(currentRoom);
                    // TODO: 加入房間，房間API初始化完成，準備同步初始錨點
                    currentState = ImportExportState.RoomApiInitialized;

                    debug("找到房間並加入！ - InitRoomApi()");

                    return;
                }
            }
        }
    }

    /// <summary>
    /// 判斷當前用戶編號是不是所有用戶中最小的
    /// </summary>
    /// <returns></returns>
    private bool LocalUserHasLowestUserId()
    {
        if (SharingStage.Instance == null)
        {
            return false;
        }
        if (SharingStage.Instance.SessionUsersTracker != null)
        {
            List<User> currentUsers = SharingStage.Instance.SessionUsersTracker.CurrentUsers;
            for (int i = 0; i < currentUsers.Count; i++)
            {
                if (currentUsers[i].GetID() < CustomMessages240.Instance.LocalUserID)
                {
                    return false;
                }
            }
        }
        return true;
    }

    #endregion

    // Update中處理各種狀態（簡單狀態機）
    private void Update()
    {
        if (currentState != lastState)
        {
            debug("狀態變化：" + lastState.ToString() + " > " + currentState.ToString());
            lastState = currentState;
        }

        lblMsg.text = sb.ToString();

        switch (currentState)
        {
            case ImportExportState.AnchorStore_Initialized:
                // 本地錨點存儲器初始化完成
                // 如果成功加入當前會話，則開始加載房間
                if (sharingServiceReady)
                {
                    InitRoomApi();
                }
                break;
            case ImportExportState.RoomApiInitialized:
                // 房間已加載完成，開始加載錨點信息
                StartAnchorProcess();
                break;
            case ImportExportState.DataReady:
                // 錨點數據下載完成後，開始導入錨點數據
                currentState = ImportExportState.Importing;
                WorldAnchorTransferBatch.ImportAsync(rawAnchorData, ImportComplete);
                break;
            case ImportExportState.InitialAnchorRequired:
                // 房主房間創建完成後，需要創建初始錨點共享給他人
                currentState = ImportExportState.CreatingInitialAnchor;
                // 創建本地錨點
                CreateAnchorLocally();
                break;
            case ImportExportState.ReadyToExportInitialAnchor:
                // 準備導出初始錨點
                currentState = ImportExportState.UploadingInitialAnchor;
                // 執行導出
                Export();
                break;
        }
    }

    /// <summary>
    /// 房主將本地錨點共享給其他人
    /// </summary>
    private void Export()
    {
        // 獲取錨點，這個組件會在CreateAnchorLocally()中自動添加
        WorldAnchor anchor = GetComponent<WorldAnchor>();

        anchorStore.Clear();
        // 本地保存該錨點
        if (anchor != null && anchorStore.Save(ExportingAnchorName, anchor))
        {
            debug("保存錨點完成，準備導出！ - Export()");
            // 將錨點導出
            sharedAnchorInterface = new WorldAnchorTransferBatch();
            sharedAnchorInterface.AddWorldAnchor(ExportingAnchorName, anchor);
            WorldAnchorTransferBatch.ExportAsync(sharedAnchorInterface, WriteBuffer, ExportComplete);
        }
        else
        {
            debug("保存本地錨點失敗！ - Export()");

            currentState = ImportExportState.InitialAnchorRequired;
        }
    }

    /// <summary>
    /// 房主導出錨點成功
    /// </summary>
    /// <param name="completionReason"></param>
    private void ExportComplete(SerializationCompletionReason completionReason)
    {
        if (completionReason == SerializationCompletionReason.Succeeded && exportingAnchorBytes.Count > minTrustworthySerializedAnchorDataSize)
        {
            // 將錨點數據上傳至當前房間中
            roomManager.UploadAnchor(
                currentRoom,
                new XString(ExportingAnchorName),
                exportingAnchorBytes.ToArray(),
                exportingAnchorBytes.Count);
        }
        else
        {
            debug("導出錨點出錯！" + completionReason.ToString());
            currentState = ImportExportState.InitialAnchorRequired;
        }
    }

    private void WriteBuffer(byte[] data)
    {
        exportingAnchorBytes.AddRange(data);
    }

    /// <summary>
    /// 房主在本地創建一個新的錨點
    /// </summary>
    private void CreateAnchorLocally()
    {
        debug("開始創建本地錨點");

        // 添加世界錨點組件
        WorldAnchor anchor = GetComponent<WorldAnchor>();
        if (anchor == null)
        {
            anchor = gameObject.AddComponent<WorldAnchor>();
        }

        if (anchor.isLocated)
        {
            // 房主自己定位好本地錨點後，準備導出給其他人
            currentState = ImportExportState.ReadyToExportInitialAnchor;
        }
        else
        {
            anchor.OnTrackingChanged += WorldAnchorForExport_OnTrackingChanged;
        }
    }

    private void WorldAnchorForExport_OnTrackingChanged(WorldAnchor self, bool located)
    {
        if (located)
        {
            // 房主自己定位好本地錨點後，準備導出給其他人
            currentState = ImportExportState.ReadyToExportInitialAnchor;
        }
        else
        {
            // 房主自己的錨點定位失敗，則同步總體失敗
            currentState = ImportExportState.Failed;
        }

        self.OnTrackingChanged -= WorldAnchorForExport_OnTrackingChanged;
    }

    /// <summary>
    /// 錨點數據下載完成後，開始導入錨點數據
    /// </summary>
    /// <param name="completionReason"></param>
    /// <param name="deserializedTransferBatch"></param>
    private void ImportComplete(SerializationCompletionReason completionReason, WorldAnchorTransferBatch deserializedTransferBatch)
    {
        if (completionReason == SerializationCompletionReason.Succeeded && deserializedTransferBatch.GetAllIds().Length > 0)
        {
            // 成功導入錨點
            // 獲取第一個錨點名稱
            bool hasAnchorName = false;
            string[] anchorNames = deserializedTransferBatch.GetAllIds();
            foreach (var an in anchorNames)
            {
                if (an == ExportingAnchorName)
                {
                    hasAnchorName = true;
                    break;
                }
            }

            if (!hasAnchorName)
            {
                currentState = ImportExportState.DataReady;
                return;
            }

            // 保存錨點到本地
            WorldAnchor anchor = deserializedTransferBatch.LockObject(ExportingAnchorName, gameObject);
            if (anchor.isLocated)
            {
                if (anchorStore.Save(ExportingAnchorName, anchor))
                {
                    currentState = ImportExportState.Ready;
                }
                else
                {
                    currentState = ImportExportState.DataReady;
                }

            }
            else
            {
                anchor.OnTrackingChanged += WorldAnchorForImport_OnTrackingChanged;
            }
        }
        else
        {
            // 未成功導入，則設置為DataReady，準備在下一幀再次導入，直到導入完成
            currentState = ImportExportState.DataReady;
        }
    }

    private void WorldAnchorForImport_OnTrackingChanged(WorldAnchor self, bool located)
    {
        if (located)
        {
            WorldAnchor anchor = GetComponent<WorldAnchor>();
            if (anchorStore.Save(ExportingAnchorName, anchor))
            {
                currentState = ImportExportState.Ready;
            }
            else
            {
                currentState = ImportExportState.DataReady;
            }
        }
        else
        {
            currentState = ImportExportState.Failed;
        }

        self.OnTrackingChanged -= WorldAnchorForImport_OnTrackingChanged;
    }

    /// <summary>
    /// 加載錨點信息
    /// </summary>
    private void StartAnchorProcess()
    {
        debug("正在獲取房間錨點…… - StartAnchorProcess()");

        // 檢查當前房間有無錨點
        int anchorCount = currentRoom.GetAnchorCount();

        if (anchorCount > 0)
        {
            bool isRoomAnchorExists = false;

            for (int i = 0; i < anchorCount; i++)
            {
                string roomAnchor = currentRoom.GetAnchorName(i).GetString();
                if (roomAnchor == ExportingAnchorName)
                {
                    isRoomAnchorExists = true;
                    break;
                }
            }

            if (isRoomAnchorExists)
            {
                debug("獲取房間錨點成功！開始下載錨點");
                // 獲取房間錨點信息成功後，開始下載錨點數據
                MakeAnchorDataRequest();
            }
        }
    }

    protected override void OnDestroy()
    {
        if (SharingStage.Instance != null)
        {
            SharingStage.Instance.SharingManagerConnected -= Instance_SharingManagerConnected;
            if (SharingStage.Instance.SessionsTracker != null)
            {
                SharingStage.Instance.SessionsTracker.CurrentUserJoined -= SessionsTracker_CurrentUserJoined;
                SharingStage.Instance.SessionsTracker.CurrentUserLeft -= SessionsTracker_CurrentUserLeft;
            }
        }

        if (roomManagerCallbacks != null)
        {
            roomManagerCallbacks.AnchorsDownloadedEvent -= RoomManagerCallbacks_AnchorsDownloadedEvent;
            roomManagerCallbacks.AnchorUploadedEvent -= RoomManagerCallbacks_AnchorUploadedEvent;

            if (roomManager != null)
            {
                roomManager.RemoveListener(roomManagerCallbacks);
            }

            roomManagerCallbacks.Dispose();
            roomManagerCallbacks = null;
        }

        if (roomManager != null)
        {
            roomManager.Dispose();
            roomManager = null;
        }

        base.OnDestroy();
    }
}