﻿using HoloToolkit.Sharing;
using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class Cube240 : MonoBehaviour, IInputClickHandler
{

    // 是否正在移動
    bool isMoving = false;

    // 消息傳遞類
    CustomMessages240 customMessage;

    private void Start()
    {
        customMessage = CustomMessages240.Instance;

        // 指定收到 Cube 位置變化消息後的處理方法
        customMessage.MessageHandlers[CustomMessages240.CustomMessageID.CubePosition] = OnCubePositionReceived;
    }

    // 收到 Cube 座标
    private void OnCubePositionReceived(NetworkInMessage msg)
    {
        Debug.Log("OnCubePositionReceived 收到 Cube 座标");

        // 同步Cube位置
        if (!isMoving)
        {
            transform.localPosition = CustomMessages240.ReadCubePosition(msg);
        }
    }

    // ------------------------------------------------------------
    // 繼承 IInputClickHandler 點擊 Cube 會觸發 OnInputClicked 函式
    // ------------------------------------------------------------

    // 單擊 Cube，切換是否移動
    public void OnInputClicked(InputClickedEventData eventData)
    {
        Debug.Log("OnInputClicked 單擊 Cube");

        isMoving = !isMoving;
        // 放置 Cube 後，發送Cube的位置消息給其他人
        if (!isMoving)
        {
            customMessage.SendCubePosition(transform.localPosition);
        }
    }

    // 如果 Cube 為移動狀態，讓其放置在鏡頭前 2 米位置
    void Update()
    {
        if (isMoving)
        {
            transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2f;
            customMessage.SendCubePosition(transform.localPosition, MessageReliability.UnreliableSequenced);
        }
    }
}